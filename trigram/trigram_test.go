// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package trigram

import (
	"reflect"
	"testing"
)

func TestExtract(t *testing.T) {
	testCases := []struct {
		in   string
		want []T
	}{
		{"", nil},
		{"еще", []T{795347, 12162634, 13078808, 3591261}},
		{"ещё", []T{795347, 12162634, 13078808, 3591261}},
		{"ЕЩЁ", []T{795347, 12162634, 13078808, 3591261}},
		{" ещё ", []T{795347, 12162634, 13078808, 3591261}},
		{"  ещё  ", []T{795347, 12162634, 13078808, 3591261}},
		{"пока ещё", []T{9401719, 610828, 13225821, 10698492, 12380791, 4418568, 12162634, 13078808, 3591261}},
		{"пока  ещё", []T{9401719, 610828, 13225821, 10698492, 12380791, 4418568, 12162634, 13078808, 3591261}},
	}
	for _, tc := range testCases {
		if got := Extract(tc.in); !reflect.DeepEqual(got, tc.want) {
			t.Errorf("Extract(%q): want %v, got %v", tc.in, tc.want, got)
		}
	}
}

func BenchmarkExtract(b *testing.B) {
	ss := []string{
		"Машенька",
		"Король, дама, валет",
		"Защита Лужина",
		"Подвиг",
		"Камера обскура",
		"Отчаяние",
		"Приглашение на казнь",
		"Дар",
	}
	n := 0
	for i := 0; i <= b.N; i++ {
		Extract(ss[n%len(ss)])
		n++
	}
}
