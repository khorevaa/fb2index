module gitlab.com/opennota/fb2index

require (
	github.com/jmoiron/sqlx v0.0.0-20180614180643-0dae4fefe7c0
	github.com/mattn/go-sqlite3 v1.9.0
	golang.org/x/text v0.3.0
)
