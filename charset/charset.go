// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package charset provides a function to generate charset-conversion readers.
package charset

import (
	"fmt"
	"io"
	"strings"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
)

// The actual FB2 XML encodings in the wild.
var nameMap = map[string]encoding.Encoding{
	"utf-8":        unicode.UTF8,
	"windows-1251": charmap.Windows1251,
	"windows-1252": charmap.Windows1252,
	"koi8-r":       charmap.KOI8R,
	"iso-8859-1":   charmap.ISO8859_1,
	"iso-8859-5":   charmap.ISO8859_5,
	"utf-16":       unicode.UTF16(unicode.LittleEndian, unicode.UseBOM),
}

// NewReader returns a new charset-conversion reader, converting from the provided charset into UTF-8.
func NewReader(label string, input io.Reader) (io.Reader, error) {
	e := nameMap[strings.ToLower(strings.TrimSpace(label))]
	if e == nil {
		return nil, fmt.Errorf("unsupported charset: %q", label)
	}
	return transform.NewReader(input, e.NewDecoder()), nil
}
